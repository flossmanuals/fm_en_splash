AddType application/epub+zip .epub
Options Multiviews FollowSymLinks
RewriteEngine on

#RewriteCond  ^(index\.html) [NC]
RewriteRule (.*)/$ http://write.flossmanuals.net/$1/ [L,R=301]



#RewriteBase /
#RewriteCond %{HTTP_HOST} !^en\.[a-z-]+\.[a-z]{2,6} [NC]
#RewriteCond %{HTTP_HOST} ([a-z-]+\.[a-z]{2,6})$ [NC]
#RewriteRule ^(/)$ /index.php [R=301,L]

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !(\.[a-zA-Z0-9]{1,5}|/)$
RewriteRule (.*/?)$ http://write.flossmanuals.net/$1/ [R=301,L]


#RewriteRule (.*)$ http://write.flossmanuals.net/$1/ [R=301,L]


#RewriteRule  ^/$  /bookipublisher/index.php [NC,L]
#RewriteRule  http://en.flossmanuals.net(/)$  /index.php [NC,L]
#RewriteRule  http://www.flossmanuals.net(/)$  /index.php [NC,L]

RewriteRule ^rss(/)$ http://www.flossmanuals.org/rss.xml [R=301,L]

# 
# edited by Mick 4/7/15
RewriteRule ^csound/introduction/index(/)? http://booki.flossmanuals.net/csound/intro/ [R=301,L]
RewriteRule ^_booki/csound/csound\.pdf$ http://floss.booktype.pro/csound/preface/ [R=301,L]
RewriteRule ^_booki/csound/csound\.epub$ http://floss.booktype.pro/csound/preface/ [R=301,L]
#RewriteRule ^csound/(.*) http://floss.booktype.pro/csound/$1
RewriteRule ^csound/ http://floss.booktype.pro/csound/preface/


# Requested by Michael McAndrew 26/6/14, via Mick
RewriteRule ^civicrm/introduction/index(/)? http://book.civicrm.org/user/ [R=301,L]
RewriteRule ^_booki/civicrm/civicrm\.pdf$ http://book.civicrm.org/user/ [R=301,L]
RewriteRule ^_booki/civicrm/civicrm\.epub$ http://book.civicrm.org/user/ [R=301,L]
RewriteRule ^civicrm/(.*) http://book.civicrm.org/user/$1

##################################################################
# Requested by Daniel James 5/8/14, via email
##################################################################
# booktype: pdf link, epub link, and html pages
RewriteRule ^_booki/booktype/booktype\.pdf$ http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
RewriteRule ^booktype/_booki/booktype/booktype\.epub$ http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
RewriteRule ^booktype/(.*) http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
#
# booktype-en-1-5 and booktype-en-1-6: epub/pdf link and html pages
RewriteRule ^_booki/booktype-en-1-[56]/booktype-en-1-[56].* http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
RewriteRule ^booktype-en-1-[56]/(.*) http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
#
# Booki-User-Guide: epub/pdf links and html pages
RewriteRule ^_booki/Booki-User-Guide/Booki-User-Guide.* http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
RewriteRule ^[bB]ooki-[uU]ser-[gG]uide/(.*) http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
#
# booki: html pages only because epub and pdf links broken on fm.net!
RewriteRule ^booki/(.*) http://sourcefabric.booktype.pro/booktype-20-for-authors-and-publishers/what-is-booktype/ [R=301,L]
#
# booktype-es: epub/pdf links and html pages
RewriteRule ^_booki/booktype-es/booktype-es.* http://sourcefabric.booktype.pro/booktype-16-para-autores-y-editores/ [R=301,L]
RewriteRule ^booktype-es/(.*) http://sourcefabric.booktype.pro/booktype-16-para-autores-y-editores/ [R=301,L]
#
# booktype-ru: epub/pdf links and html pages
RewriteRule ^booktype-ru/_booki/booktype-ru/booktype-ru.* http://sourcefabric.booktype.pro/booktype-16-dlia-avtorov-i-izdatelei/ [R=301,L]
RewriteRule ^booktype-ru/(.*) http://sourcefabric.booktype.pro/booktype-16-dlia-avtorov-i-izdatelei/ [R=301,L]
#
# gen-live-desk and live-blog-en-1-0: epub/pdf links and html pages
RewriteRule ^(gen-live-desk)|(live-blog-en-1-0)/_booki/(gen-live-desk)|(live-blog-en-1-0)/(gen-live-desk)|(live-blog-en-1-0).* http://sourcefabric.booktype.pro/live-blog-20-for-journalists/ [R=301,L]
RewriteRule ^(gen-live-desk)|(live-blog-en-1-0)/(.*) http://sourcefabric.booktype.pro/live-blog-20-for-journalists/ [R=301,L]
#
# newscoop-ru and newscoop-4-journalists-ru-4-0: epub/pdf links and html pages
RewriteRule ^(newscoop-ru)|(newscoop-4-journalists-ru-4-0)/_booki/(newscoop-ru)|(newscoop-4-journalists-ru-4-0)/(newscoop-ru)|(newscoop-4-journalists-ru-4-0).* http://sourcefabric.booktype.pro/newscoop-41-dlia-zhurnalistov-i-redaktorov/ [R=301,L]
RewriteRule ^(newscoop-ru)|(newscoop-4-journalists-ru-4-0)/(.*) http://sourcefabric.booktype.pro/newscoop-41-dlia-zhurnalistov-i-redaktorov/ [R=301,L]
#
# airtime and airtime-en-2-[0123]: epub/pdf links and html pages
RewriteRule ^_booki/airtime(-en-2-[0123])?/airtime(-en-2-[0123])?.* http://sourcefabric.booktype.pro/airtime-25-for-broadcasters/ [R=301,L]
RewriteRule ^airtime(-en-2-[0123])?/(.*) http://sourcefabric.booktype.pro/airtime-25-for-broadcasters/ [R=301,L]
#
# airtime-es-2-[01]: epub/pdf links and html pages
RewriteRule ^_booki/airtime(-es-2-[01])?/airtime(-es-2-[01])?.* http://sourcefabric.booktype.pro/airtime-25-para-estaciones-de-radio/ [R=301,L]
RewriteRule ^airtime(-es-2-[01])?/(.*) http://sourcefabric.booktype.pro/airtime-25-para-estaciones-de-radio/ [R=301,L]
#
# airtime-ru-2-3: epub/pdf links and html pages
RewriteRule ^_booki/airtime-ru-2-3/airtime-ru-2-3\.* http://sourcefabric.booktype.pro/airtime-23-dlia-radiostantsii [R=301,L]
RewriteRule ^airtime-ru-2-3/(.*) http://sourcefabric.booktype.pro/airtime-23-dlia-radiostantsii/ [R=301,L]
#
# newscoop-4-cookbook-en-4-0 and newscoop-4-cookbook-en-4-1: epub/pdf link and html pages
RewriteRule ^_booki/newscoop-4-cookbook-en-4-[01]/newscoop-4-cookbook-en-4-[01].* http://sourcefabric.booktype.pro/newscoop-42-cookbook/ [R=301,L]
RewriteRule ^newscoop-4-cookbook-en-4-[01]/(.*) http://sourcefabric.booktype.pro/newscoop-42-cookbook/ [R=301,L]
#
# newscoop-4-journalists-en-4-0 and newscoop-4-journalists-en-4-1: epub/pdf link and html pages
RewriteRule ^_booki/newscoop-4-journalists-en-4-[01]/newscoop-4-journalists-en-4-[01].* http://sourcefabric.booktype.pro/newscoop-43-for-journalists-and-editors/ [R=301,L]
RewriteRule ^newscoop-4-journalists-en-4-[01]/(.*) http://sourcefabric.booktype.pro/newscoop-43-for-journalists-and-editors/ [R=301,L]
#
# newscoop-4-journalists-de-4-0: epub/pdf link and html pages
RewriteRule ^_booki/newscoop-4-journalists-de-4-0/newscoop-4-journalists-de-4-0.* http://sourcefabric.booktype.pro/newscoop-41-fur-journalisten-und-redakteure/ [R=301,L]
RewriteRule ^newscoop-4-journalists-de-4-0/(.*) http://sourcefabric.booktype.pro/newscoop-41-fur-journalisten-und-redakteure/ [R=301,L]
#
# newscoop-4-journalists-es-4-0: epub/pdf link and html pages
RewriteRule ^_booki/newscoop-4-journalists-es-4-0/newscoop-4-journalists-es-4-0.* http://sourcefabric.booktype.pro/newscoop-41-para-periodistas-y-editores/ [R=301,L]
RewriteRule ^newscoop-4-journalists-es-4-0/(.*) http://sourcefabric.booktype.pro/newscoop-41-para-periodistas-y-editores/ [R=301,L]
##################################################################



#RewriteRule  ^([A-Za-z0-9-_]*)/([A-Za-z0-9_-]*)(.html)/?$ /bookipublisher/index.php?book=$1&chapter=$2 [NC,L]
#RewriteRule  ^([A-Za-z0-9-_]*)/?$ /bookipublisher/index.php?book=$1&chapter=index [NC,L]
#RewriteRule  http://en.flossmanuals.net/([A-Za-z0-9-_]*)/?$ /bookipublisher/index.php?book=$1&chapter=index [NC,L]

