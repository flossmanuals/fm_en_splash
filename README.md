FLOSS Manuals EN Splash
A splash page for FLOSS Manuals to replace current defunct publisher


##Process


The process for publishing to this front page is quite low tech.

You can either do a git pull request if you are up for that and add your
details to this file.
https://gitlab.com/mickfuzz/fm_en_splash/blob/master/data/book_details.json

Or just email the following details and image to me. mick@flossmanuals.net
Image size is 600x312 and there's an inkscape template here -
https://gitlab.com/mickfuzz/fm_en_splash/tree/master/svg

Just replace with the appropriate text and links.

As you can see this makes the process a bit messier but a lot more
decentralised and flexible.

Sample entry Below

    {   "title": "Plumi",
        "category": "Video",
        "img_url":
        "http://splash.flossmanuals.net/index_fr_files/plumi_600.png",
        "description": "Plumi enables you to create your own
    sophisticated video-sharing site. It includes a beautiful adaptive skin
    using Diazo, server-side transcoding of most video formats, upload
    progress bar, thumbnail extraction, HTML5 video playback and embedding,
    subtitles using Amara, large file uploading via FTP, social media
    integration, threaded commenting and user feedback forms, customised
    user profiles and a range of other useful features.",
        "read_link": "http://write.flossmanuals.net/plumi/",
        "pdf_link":
    "http://en.flossmanuals.net/plumi/_booki/plumi/plumi.pdf",
        "epub_link":
    "http://en.flossmanuals.net/plumi/_booki/plumi/plumi.epub",
        "languages": [
            {"name": "English",
            "link": "http://write.flossmanuals.net/plumi"}
        ],        

        "keywords": [
            {"name": "Video Blogging"},
            {"name": "CMS"}, {
            "name": "Selfhosting"}
    ]},
   

